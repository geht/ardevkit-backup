Take a look at [this pdf](https://bitbucket.org/geht/ardevkit-backup/downloads/ARdevKit.pdf) for a general overview of this project.

Original project pages:

* [ARdevKit](https://github.com/ImanuelRichter/ARdevKit)
* [ARdevKitPlayer](https://github.com/ImanuelRichter/ARdevKitPlayer)

For a list of features take a look at the [releases](https://github.com/ImanuelRichter/ARdevKit/releases)

Also take a look at the [download ](https://bitbucket.org/geht/ardevkit-backup/downloads) section, where you can find some documents related to this project.